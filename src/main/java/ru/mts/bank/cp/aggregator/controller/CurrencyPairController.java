package ru.mts.bank.cp.aggregator.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.mts.bank.cp.aggregator.dto.PairsDTO;
import ru.mts.bank.cp.aggregator.service.CurrencyPairService;

@RestController
@RequestMapping("/api/v1/pairs")
@RequiredArgsConstructor
public class CurrencyPairController {
    private final CurrencyPairService currencyPairService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public PairsDTO findByNameAndDateAfter(@RequestParam("name") String name,
                                           @RequestParam("date") String date) {
        return currencyPairService.findByNameAndDateAfter(name, date);
    }
}
