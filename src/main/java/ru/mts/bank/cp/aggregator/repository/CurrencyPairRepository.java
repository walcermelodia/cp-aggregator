package ru.mts.bank.cp.aggregator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mts.bank.cp.aggregator.entity.CurrencyPairEntity;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface CurrencyPairRepository extends JpaRepository<CurrencyPairEntity, Long> {
    List<CurrencyPairEntity> findAllByDateAfter(LocalDateTime date);
}
