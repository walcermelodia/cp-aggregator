package ru.mts.bank.cp.aggregator.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@AllArgsConstructor
@Data
public class CurrencyPairDTO {
    private String base;
    private String quote;
    private Double bid;
    private Double ask;
    private String date;
}
