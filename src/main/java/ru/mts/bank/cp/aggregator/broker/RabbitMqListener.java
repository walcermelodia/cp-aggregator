package ru.mts.bank.cp.aggregator.broker;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import ru.mts.bank.cp.aggregator.service.CurrencyPairService;

@EnableRabbit
@Component
@Slf4j
@RequiredArgsConstructor
public class RabbitMqListener {
    private final ObjectMapper objectMapper;
    private final CurrencyPairService currencyPairService;

    @RabbitListener(queues = "queue")
    public void listen(String message) throws JsonProcessingException {
        CurrencyPairMessage currencyPairMessage = objectMapper.readValue(message, CurrencyPairMessage.class);
        log.info("Received from queue: {}", currencyPairMessage);

        currencyPairService.save(currencyPairMessage);
    }
}
