package ru.mts.bank.cp.aggregator.broker;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CurrencyPairMessage {
    private String pairName;
    private Double bid;
    private Double ask;
    private String creationDate;
}
