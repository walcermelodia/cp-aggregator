package ru.mts.bank.cp.aggregator.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "cp_register")
@Data
@NoArgsConstructor
public class CurrencyPairEntity {
    @Id
    @SequenceGenerator(
            name = "cp_register_sequence",
            sequenceName = "cp_register_sequence",
            allocationSize = 1)
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "cp_register_sequence"
    )
    private Long id;
    private String base;
    private String quote;
    private Double bid;
    private Double ask;
    private LocalDateTime date;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(
            name = "catalog_id",
            nullable = false,
            referencedColumnName = "id")
    private CurrencyPairTypeEntity currencyPairTypeEntity;

    public CurrencyPairEntity(String base, String quote, Double bid, Double ask, LocalDateTime date) {
        this.base = base;
        this.quote = quote;
        this.bid = bid;
        this.ask = ask;
        this.date = date;
    }


}
