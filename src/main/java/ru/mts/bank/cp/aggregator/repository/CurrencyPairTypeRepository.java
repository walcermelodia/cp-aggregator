package ru.mts.bank.cp.aggregator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mts.bank.cp.aggregator.entity.CurrencyPairTypeEntity;

import java.util.Optional;

@Repository
public interface CurrencyPairTypeRepository extends JpaRepository<CurrencyPairTypeEntity, Long> {
    Optional<CurrencyPairTypeEntity> findByName(String name);
    boolean existsByName(String name);
}
