package ru.mts.bank.cp.aggregator.service;

import lombok.RequiredArgsConstructor;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mts.bank.cp.aggregator.broker.CurrencyPairMessage;
import ru.mts.bank.cp.aggregator.dto.CurrencyPairDTO;
import ru.mts.bank.cp.aggregator.dto.PairsDTO;
import ru.mts.bank.cp.aggregator.entity.CurrencyPairEntity;
import ru.mts.bank.cp.aggregator.entity.CurrencyPairTypeEntity;
import ru.mts.bank.cp.aggregator.exception.InvalidDataException;
import ru.mts.bank.cp.aggregator.exception.NotFoundException;
import ru.mts.bank.cp.aggregator.repository.CurrencyPairRepository;
import ru.mts.bank.cp.aggregator.repository.CurrencyPairTypeRepository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CurrencyPairService {
    private final CurrencyPairRepository currencyPairRepository;
    private final CurrencyPairTypeRepository currencyPairTypeRepository;
    private final DateTimeFormatter dtf;
    private final static String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public PairsDTO findByNameAndDateAfter(String name, String dateAfter) {
        if (!isValidDateFormat(dateAfter)) {
            throw new InvalidDataException(String.format("Invalid format: %s", dateAfter));
        }

        if (!currencyPairTypeRepository.existsByName(name)) {
            throw new NotFoundException(String.format("Currency Pair: %s not exists", name));
        }

        LocalDateTime dateTime = LocalDateTime.parse(dateAfter, dtf);

        Predicate<CurrencyPairEntity> filterByName = e -> e.getCurrencyPairTypeEntity().getName().equals(name);
        List<CurrencyPairEntity> entities =  currencyPairRepository.findAllByDateAfter(dateTime).stream()
                .filter(filterByName)
                .collect(Collectors.toList());
        return map(entities);
    }

    @Transactional
    public void save(CurrencyPairMessage currencyPairMessage) {
        Optional<CurrencyPairTypeEntity> currencyPairTypeOptional =
                currencyPairTypeRepository.findByName(currencyPairMessage.getPairName());

        CurrencyPairEntity entity;
        if (currencyPairTypeOptional.isPresent()) {
            entity = map(currencyPairMessage);
            entity.setCurrencyPairTypeEntity(currencyPairTypeOptional.get());
        } else {
            String name = currencyPairMessage.getPairName();
            CurrencyPairTypeEntity cpTypeEntity = new CurrencyPairTypeEntity(name);
            currencyPairTypeRepository.save(cpTypeEntity);

            CurrencyPairTypeEntity typeEntity = currencyPairTypeRepository.findByName(name).get();
            entity = map(currencyPairMessage);
            entity.setCurrencyPairTypeEntity(typeEntity);
        }
        currencyPairRepository.save(entity);
    }

    private CurrencyPairEntity map(CurrencyPairMessage message) {
        int mid = message.getPairName().length() / 2;
        String base = message.getPairName().substring(0, mid);
        String quote = message.getPairName().substring(mid);
        LocalDateTime dateTime = LocalDateTime.parse(message.getCreationDate(), dtf);
        return new CurrencyPairEntity(base, quote, message.getBid(), message.getAsk(), dateTime);
    }

    private boolean isValidDateFormat(String date) {
        return GenericValidator.isDate(date, DATE_TIME_FORMAT ,true);
    }

    private PairsDTO map(List<CurrencyPairEntity> entities) {
        String name = entities.get(0).getCurrencyPairTypeEntity().getName();
        Function<CurrencyPairEntity, CurrencyPairDTO> entityToDto = entity -> new CurrencyPairDTO(
                entity.getBase(),
                entity.getQuote(),
                entity.getBid(),
                entity.getAsk(),
                dtf.format(entity.getDate()));

        List<CurrencyPairDTO> pairDTOS = entities.stream()
                .map(entityToDto)
                .collect(Collectors.toList());
        return new PairsDTO(name, pairDTOS);
    }
}
