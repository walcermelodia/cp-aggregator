package ru.mts.bank.cp.aggregator.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
public class PairsDTO {
    private String name;
    private List<CurrencyPairDTO> pairs;
}
