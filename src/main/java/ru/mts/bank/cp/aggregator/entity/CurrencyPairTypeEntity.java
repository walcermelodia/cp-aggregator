package ru.mts.bank.cp.aggregator.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "cp_catalog")
@Data
@NoArgsConstructor
public class CurrencyPairTypeEntity {
    @Id
    @SequenceGenerator(
            name = "cp_catalog_sequence",
            sequenceName = "cp_catalog_sequence",
            allocationSize = 1)
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "cp_catalog_sequence")
    private Long id;
    @Column(name = "name_pair")
    private String name;

    public CurrencyPairTypeEntity(String name) {
        this.name = name;
    }
}
