package ru.mts.bank.cp.aggregator.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.mts.bank.cp.aggregator.dto.PairsDTO;
import ru.mts.bank.cp.aggregator.service.CurrencyPairService;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CurrencyPairControllerTest {
    @Mock
    private CurrencyPairService service;
    @InjectMocks
    private CurrencyPairController controller;
    @Test
    void findByNameAndDateAfter() {
        PairsDTO dto = mock(PairsDTO.class);
        when(service.findByNameAndDateAfter("name", "date")).thenReturn(dto);

        PairsDTO actual = controller.findByNameAndDateAfter("name", "date");

        assertNotNull(actual);
        assertEquals(dto, actual);
        verify(service).findByNameAndDateAfter("name", "date");
    }
}