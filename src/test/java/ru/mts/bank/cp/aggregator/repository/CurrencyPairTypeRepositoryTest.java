package ru.mts.bank.cp.aggregator.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import ru.mts.bank.cp.aggregator.entity.CurrencyPairTypeEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@ActiveProfiles("development")
class CurrencyPairTypeRepositoryTest {
    @Autowired
    private CurrencyPairTypeRepository repository;
    @Autowired
    private TestEntityManager entityManager;
    private final static String TEST_NAME = "EURUSD";
    @BeforeEach
    void setUp() {
        CurrencyPairTypeEntity entity = new CurrencyPairTypeEntity(TEST_NAME);
        entityManager.persist(entity);
    }

    @Test
    public void findByName_shouldFind_whenExist() {
        CurrencyPairTypeEntity actual = repository.findByName(TEST_NAME).get();

        assertEquals(TEST_NAME, actual.getName());
    }

    @Test
    public void existsByName_shouldReturnTrue_whenExists() {
        boolean actual = repository.existsByName(TEST_NAME);

        assertTrue(actual);
    }
}